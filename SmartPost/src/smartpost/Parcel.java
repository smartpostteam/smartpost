package smartpost;

/**
 * Created by Igor Soroka on 06.12.2015.
 */

public abstract class Parcel {
    protected double length; //length of the parcel
    protected double width; //width of the parcel
    protected double height; //height of the parcel
    protected double weight; //weight of the parcel
    protected double distance; //distance for travelling
    protected boolean fragile; //the parcel fragile or not
    protected float lon1;//from longitude
    protected float lat1;//frome latitue
    protected float lon2;//to longitude
    protected float lat2;//to latitude
    protected int speed;//parcel class for path script
    protected String name;

    protected double lengthLimit; //limit in length
    protected double widthLimit; //limit in width
    protected double heightLimit; //limit in height
    protected double weightLimit; //limit in weight
    protected double distanceLimit; //limit in distance
    protected boolean machineBreakOrNot; ////can machine break the parcel or not

    //Setters for limits
    public abstract double setLengthLimit();
    public abstract double setWidthLimit();
    public abstract double setHeightLimit();
    public abstract double setWeightLimit();
    public abstract double setDistanceLimit();
    public abstract boolean setMachineBreakOrNot();

    public Parcel(String name, double length, double width, double height, double weight, 
            double distance, boolean fragile, float lon1, float lat1, float lon2 ,float lat2/*,ArrayList coordinates*/,int speed ) 
    {
        this.name = name;
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.distance = distance;
        this.fragile = fragile;
        this.lon1 = lon1;
        this.lat1 = lat1;
        this.lon2 = lon2;
        this.lat2 = lat2;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }
  
    public float getLon1() {
        return lon1;
    }

    public float getLat1() {
        return lat1;
    }

    public float getLon2() {
        return lon2;
    }

    public float getLat2() {
        return lat2;
    }
    
    public int getSpeed(){
        return speed;
    }
    @Override
    public String toString(){
        return name;
    }
}

class FirstClassParcel extends Parcel {
      
    public int getSpeed() {
    return speed;
    }  

    public FirstClassParcel(String name, double length, double width, double height, double weight, 
            double distance, boolean fragile, float lon1, float lat1, float lon2 ,float lat2) {
        super(name, length, width, height, weight, distance, fragile, lon1, lat1, lon2, lat2, 1);
    }

    @Override
    public double setLengthLimit() {
        return lengthLimit = 60.0;
    }
    
    @Override
    public double setWidthLimit() {
        return widthLimit = 36.0;
    }

    @Override
    public double setHeightLimit() {
        return heightLimit = 20.0;
    }

    @Override
    public double setWeightLimit() {
        return weightLimit = 35.0;
    }

    @Override
    public double setDistanceLimit() {
        return distanceLimit = 150.0;
    }

    @Override
    public boolean setMachineBreakOrNot() {
        return machineBreakOrNot = true;
    }
}

class SecondClassParcel extends Parcel {
    
    public SecondClassParcel(String name, double length, double width, double height, double weight, 
            double distance, boolean fragile, float lon1, float lat1, float lon2 ,float lat2) {
        super(name, length, width, height, weight, distance, fragile, lon1, lat1, lon2, lat2, 2);
    }       

    @Override
    public double setLengthLimit() {
        return lengthLimit = 60.0;
    }
    
    @Override
    public double setWidthLimit() {
        return widthLimit = 36.0;
    }

    @Override
    public double setHeightLimit() {
        return heightLimit = 38.0;
    }

    @Override
    public double setWeightLimit() {
        return weightLimit = 35.0;
    }

    @Override
    public double setDistanceLimit() {
        return distanceLimit = 5000;
    }

    @Override
    public boolean setMachineBreakOrNot() {
        return machineBreakOrNot = false;
    }
}

class ThirdClassParcel extends Parcel {
    public ThirdClassParcel(String name, double length, double width, double height, double weight, 
            double distance, boolean fragile, float lon1, float lat1, float lon2 ,float lat2) {
        super(name, length, width, height, weight, distance, fragile, lon1, lat1, lon2, lat2,3);
    }

    @Override
    public double setLengthLimit() {
        return lengthLimit = 60.0;
    }
    
    @Override
    public double setWidthLimit() {
        return widthLimit = 36.0;
    }

    @Override
    public double setHeightLimit() {
        return heightLimit = 60.0;
    }

    @Override
    public double setWeightLimit() {
        return weightLimit = 35.0;
    }

    @Override
    public double setDistanceLimit() {
        return distanceLimit = 5000.0;
    }

    @Override
    public boolean setMachineBreakOrNot() {
        return machineBreakOrNot = true;
    }
}
