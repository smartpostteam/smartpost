/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

/**
 *
 * @author Olga
 */
import java.io.BufferedReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public final class SmartPostMachine {

    private Document doc;
    private ArrayList<SmartPost> spms;
    private String content;
    private ObservableList<String> places = FXCollections.observableArrayList();
    
    public ArrayList<SmartPost> getSpms() {
        return spms;
    }

    public ObservableList<String> getPlaces() {
        return places;
    }
    
    public SmartPostMachine() throws IOException {
        this.spms = new ArrayList();
        //This part is for getting information from content of http://smartpost.ee/fi_apt.xml
        urlGetter("http://smartpost.ee/fi_apt.xml");
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            doc = documentBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();

            NodeList listOfPlaces = doc.getElementsByTagName("place");
            int totalPlaces = listOfPlaces.getLength();
            for (int i = 0; i < listOfPlaces.getLength(); i++) {
                Node firstPlaceNode = listOfPlaces.item(i);
                if(firstPlaceNode.getNodeType() == Node.ELEMENT_NODE){
                    //Getting first place element
                    Element firstPlaceElement = (Element)firstPlaceNode;
                    //Getting codes from the xml file
                    NodeList codesList = firstPlaceElement.getElementsByTagName("code");
                    Element codeElement = (Element)codesList.item(0);
                    NodeList textCodeList = codeElement.getChildNodes();
                    String placeListItem = ((Node)textCodeList.item(0)).getNodeValue().trim();
                    //Getting names of the cities from xml
                    NodeList citiesList = firstPlaceElement.getElementsByTagName("city");
                    Element cityElement = (Element)citiesList.item(0);
                    NodeList textCityList = cityElement.getChildNodes();
                    String cityListItem = ((Node)textCityList.item(0)).getNodeValue().trim();
                    //Getting addresses from xml
                    NodeList addressesList = firstPlaceElement.getElementsByTagName("address");
                    Element addressElement = (Element)addressesList.item(0);
                    NodeList textAddressList = addressElement.getChildNodes();
                    String addressListItem = ((Node)textAddressList.item(0)).getNodeValue().trim();
                    //Getting availability hours
                    NodeList avaList = firstPlaceElement.getElementsByTagName("availability");
                    Element avaElement = (Element)avaList.item(0);
                    NodeList textAvaList = avaElement.getChildNodes();
                    String avaListItem = ((Node)textAvaList.item(0)).getNodeValue().trim();
                    //Getting post offices
                    NodeList postList = firstPlaceElement.getElementsByTagName("postoffice");
                    Element postElement = (Element)postList.item(0);
                    NodeList textPostList = postElement.getChildNodes();
                    String postListItem = ((Node)textPostList.item(0)).getNodeValue().trim();
                    //Getting coordinates of the smartpost (latitude)
                    NodeList latList = firstPlaceElement.getElementsByTagName("lat");
                    Element latElement = (Element)latList.item(0);
                    NodeList textLatList = latElement.getChildNodes();
                    String latListItem = ((Node)textLatList.item(0)).getNodeValue().trim();
                    //Getting coordinates of the smartpost (longitude)
                    NodeList lngList = firstPlaceElement.getElementsByTagName("lng");
                    Element lngElement = (Element)lngList.item(0);
                    NodeList textLngList = lngElement.getChildNodes();
                    String lngListItem = ((Node)textLngList.item(0)).getNodeValue().trim();
                    //Creating object SmartPost
                    spms.add(new SmartPost(placeListItem, cityListItem, addressListItem, 
                            avaListItem, postListItem, latListItem, lngListItem));
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < getSpms().size(); i++) {
            String fullAddress = getSpms().get(i).getCity() + " , " + getSpms().get(i).getAddress();
            this.places.add(fullAddress);
        }
    }
    void urlGetter(String urlName) throws IOException{
        URL url = new URL(urlName); //url with areas
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        this.content = "";
        String line;
        while((line = br.readLine()) != null) {
            this.content += line + "\n";
        }
    }

}
