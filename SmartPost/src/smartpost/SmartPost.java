/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

/**
 *
 * @author Olga
 */
public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postOffice;
    private String latCoordinate;
    private String lngCoordinate;
    
    SmartPost(String code, String city, String address, String availability, 
            String postOffice, String latCoordinate, String lngCoordinate){
        this.code = code;
        this.city = city;
        this.address = address;
        this.availability = availability;
        this.postOffice = postOffice;
        this.latCoordinate = latCoordinate;
        this.lngCoordinate = lngCoordinate;
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public String getLatCoordinate() {
        return latCoordinate;
    }

    public String getLngCoordinate() {
        return lngCoordinate;
    }
    
}
