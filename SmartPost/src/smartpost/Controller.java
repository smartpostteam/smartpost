/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebView;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Created by User on 02.12.2015.
 */
public class Controller {

    @FXML
    private Button createPackageButton;
    @FXML
    private Button removeRouteButton;
    @FXML
    private Button updatePackageButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button addToMapButton;
    @FXML
    private WebView mapWindow;
    @FXML
    private ComboBox placeChooser;
    @FXML
    private ComboBox<Parcel> classChooser;

    SmartPostMachine spm;
    int intPlaceChoice;
    String startMapAddressCoordsLat;
    String startMapAddressCoordsLng;
    String endMapAddressCoordsLat;
    String endMapAddressCoordsLng;
    int counterAddresses = 0;
    Storage storage;
       
    public void initialize() throws IOException{
        //Load map and create storage
        mapWindow.getEngine().load(getClass().getResource("index.html").toExternalForm());
        spm = new SmartPostMachine();
        placeChooser.setItems(spm.getPlaces());
        placeChooser.getSelectionModel().selectedIndexProperty().addListener
        ((ObservableValue<? extends Number> observableValue, Number number, Number t1) -> {
            intPlaceChoice = t1.intValue();
        });
        
        storage = Storage.getInstance();    
        }
    
    @FXML
    private void chooseParcelAction(MouseEvent event) {
        //By clicking mouse to combobox fills combobox by parcels from storage
        classChooser.getItems().clear();
        for(int i = 0; i < storage.getParcels().size(); i++)
            classChooser.getItems().add(i, storage.getParcels().get(i));
    }
        
    @FXML
    public void removeRouteAction(ActionEvent actionEvent) {
        mapWindow.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    public void updateAction(ActionEvent actionEvent) {
        //Reload map and clean choice for places and parcels
        String location = mapWindow.getEngine().getLocation();
        mapWindow.getEngine().load(location);
        placeChooser.valueProperty().set("Choose City");
        classChooser.valueProperty().setValue(null);       
    }
    
    @FXML
    public void sendAction(ActionEvent actionEvent) {
        //Draw parcel path to the map. Path's color depends of the parcel's class
        try {
            Parcel parcel;
            parcel = (Parcel)classChooser.getValue();
            ArrayList<Float> al = new ArrayList<>();        
            al.add(parcel.getLon1());
            al.add(parcel.getLat1());
            al.add(parcel.getLon2());
            al.add(parcel.getLat2());
            
            if(parcel.speed == 1){  
                mapWindow.getEngine().executeScript("document.createPath(" + al + ", 'red', 1)");
                counterAddresses = 0; 
                }
            if(parcel.speed == 2){   
                mapWindow.getEngine().executeScript("document.createPath(" + al + ", 'green', 2)");
                counterAddresses = 0; 
                }               
            if(parcel.speed == 3){ 
                mapWindow.getEngine().executeScript("document.createPath(" + al + ", 'blue', 3)");
                counterAddresses = 0; 
                } 
            } catch (Exception e) {
                Alert myalert2 = new Alert(AlertType.INFORMATION, "Choose package first");
                myalert2.showAndWait();
            }
    }
    @FXML
    public void addAction(ActionEvent actionEvent) {
        //Address in format: Street and building, postal code, city
        if (counterAddresses == 0) {
            String startMapAddress = "document.goToLocation(\'" + spm.getSpms().get(intPlaceChoice).getAddress() + ", " +
                    spm.getSpms().get(intPlaceChoice).getCode() + ", " + spm.getSpms().get(intPlaceChoice).getCity() + "\', "
                    + "\'Open: " + spm.getSpms().get(intPlaceChoice).getAvailability() + "\', 'green')";
            startMapAddressCoordsLat = spm.getSpms().get(intPlaceChoice).getLatCoordinate();
            startMapAddressCoordsLng = spm.getSpms().get(intPlaceChoice).getLngCoordinate();
            mapWindow.getEngine().executeScript(startMapAddress);
            counterAddresses++;
        }
        else {
            String endMapAddress = "document.goToLocation(\'" + spm.getSpms().get(intPlaceChoice).getAddress() + ", " +
                    spm.getSpms().get(intPlaceChoice).getCode() + ", " + spm.getSpms().get(intPlaceChoice).getCity() + "\', "
                    + "\'Open: " + spm.getSpms().get(intPlaceChoice).getAvailability() + "\', 'red')";
            endMapAddressCoordsLat = spm.getSpms().get(intPlaceChoice).getLatCoordinate();
            endMapAddressCoordsLng = spm.getSpms().get(intPlaceChoice).getLngCoordinate();
            mapWindow.getEngine().executeScript(endMapAddress);
            counterAddresses++;
        }
             
    }
    
    @FXML
    private void startWebViewAction(ActionEvent event) {        
        try {
            Stage webview = new Stage();            
            Parent page = FXMLLoader.load(getClass().getResource("CreatePackageWindow.fxml"));            
            Scene scene = new Scene(page);            
            webview.setScene(scene);
            webview.show();           
            
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
}
