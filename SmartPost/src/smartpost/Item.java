/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

/**
 *
 * @author Olga
 */
public class Item {
    private String name;
    private double length;
    private double width;
    private double height;
    private double weight;
    private boolean fragile;
    
    public Item(String n, double l, double w, double h, double wei, boolean b){
        name = n;
        length = l;
        width = w;
        height = h;
        weight = wei;
        fragile = b;      
    }

    public String getName(){
        return name; 
    } 
    
    public double getLength(){
        return length;       
    }
    
    public double getWidth(){
        return width;       
    }
    
    public double getHeight(){
        return height;       
    }
        
    public double getWeight(){
        return weight;       
    }
        
    public boolean getBreakable(){
        return fragile;       
    }
        
    @Override
    public String toString(){
        return name;
    }
}