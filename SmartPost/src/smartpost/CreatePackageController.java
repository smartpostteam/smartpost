/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Olga
 */
public class CreatePackageController implements Initializable {
    @FXML
    private Label createPckageLable;
    @FXML
    private Label chooseObjectLable;
    @FXML
    private ComboBox<Item> chooseObjectButton;
    @FXML
    private Label orLable;
    @FXML
    private Label creatNewObjectLable;
    @FXML
    private TextField nameField;
    @FXML
    private TextField massField;
    @FXML
    private TextField heightField;
    @FXML
    private TextField widthField;
    @FXML
    private RadioButton firstClassButton;
    @FXML
    private CheckBox breakableCheckBox;
    @FXML
    private RadioButton secondClassButton;
    @FXML
    private RadioButton thirdClassButton;
    @FXML
    private Button classInformationButton;
    @FXML
    private Label choseClassLable;
    @FXML
    private Label deliveryInformationLable;
    @FXML
    private Label infoTextLabel;
    @FXML
    private Button cancelButton;
    @FXML
    private ComboBox fromButton;   
    @FXML
    private ComboBox toButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private TextField lengthField;
    @FXML
    private WebView invisibleMap;
    SmartPostMachine spm2;
    int intFromButtonChoice;
    int intToButtonChoice;
    boolean fragile;
    String parcelName;
    
    /**
     * Initialises the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Invisible map for distance counting
        invisibleMap.getEngine().load(getClass().getResource("index.html").toExternalForm());
        //Default Items
        chooseObjectButton.getItems().add(0, new Item("ball", 20.0, 20.0, 20.0, 0.5, false));
        chooseObjectButton.getItems().add(0, new Item("sweets", 25.0, 12.0, 8.0, 0.4, true));
        chooseObjectButton.getItems().add(0, new Item("notebook", 40.0, 30.0, 10.0, 2.0, true));
        chooseObjectButton.getItems().add(0, new Item("suite", 60.0, 20.0, 44.0, 3.0, false));       

        try {
            spm2 = new SmartPostMachine();
            fromButton.setItems(spm2.getPlaces());
            toButton.setItems(spm2.getPlaces());
        
        } catch (IOException ex) {
            Logger.getLogger(CreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        fromButton.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observableValue, Number number, Number t1) -> {
            intFromButtonChoice = t1.intValue();
        });
        toButton.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observableValue, Number number, Number t2) -> {
            intToButtonChoice = t2.intValue();
        });
        
        Storage st = Storage.getInstance();
    }     

    @FXML
    private void chooseObjectAction(ActionEvent event) {
        //Fill textFields by chosen object properties
        String parcelName = chooseObjectButton.getValue().toString();
        nameField.setText(parcelName);
        lengthField.setText(Double.toString(chooseObjectButton.getValue().getLength()));
        widthField.setText(Double.toString(chooseObjectButton.getValue().getWidth()));
        heightField.setText(Double.toString(chooseObjectButton.getValue().getWidth()));
        massField.setText(Double.toString(chooseObjectButton.getValue().getWeight()));
        breakableCheckBox.setSelected(chooseObjectButton.getValue().getBreakable());                
    }

    @FXML
    private void showInfoAction(ActionEvent event) throws IOException {
            //Show info about choosen class    
            if (firstClassButton.isSelected()) {
                infoTextLabel.setText("1. class: Fast. Distance limit is 150 km. Size limit "
                        + "is 60cm x 36cm x 20cm. Weight limit is 35 kg. Not for fragile objects.");
            }
          
            else if (secondClassButton.isSelected()) {
                infoTextLabel.setText("2. class: No distance limit. Size limit is 60cm x 36cm x 38cm. "
                        + "Weigtlimit is 35 kg. Safety class.");
            }
            
            else if (thirdClassButton.isSelected()) {
                infoTextLabel.setText("3. class: No distancelimit. Slow. Size limit is "
                        + "60cm x 36cm x 60cm. Unsafe. Not for fragile objects. Slow.");
            }
            
            else {
                infoTextLabel.setText("Choose class!");
            }        
    }
    
    @FXML
    private void chooseBreakableAction(ActionEvent event) {
        breakableCheckBox.setSelected(true);
    }
    
    //Choosing of parcel class
    @FXML
    private void chooseOneAction(ActionEvent event) {
        secondClassButton.setSelected(false);
        thirdClassButton.setSelected(false);
    }

    @FXML
    private void chooseSecondAction(ActionEvent event) {
        firstClassButton.setSelected(false);
        thirdClassButton.setSelected(false);
     }

    @FXML
    private void chooseThirdAction(ActionEvent event) {
        secondClassButton.setSelected(false);
        firstClassButton.setSelected(false);
     }    
    //What class of parcel we have to create
    public int getParcelClass(){
        int returnValue = 0;    
        if(firstClassButton.isSelected()){
            returnValue = 1;
        }
        else if(secondClassButton.isSelected()){
            returnValue = 1;
        }
        else if(thirdClassButton.isSelected()){
            returnValue = 3;
        }       
        else {
            returnValue = 0;
        }       
        return returnValue;
    }
       
    @FXML
    private void cancelAction(ActionEvent event) {        
        closeWindow(cancelButton);                
    }
    
    public void closeWindow(Button button){
        Stage scene = (Stage) button.getScene().getWindow();
        scene.close();
    }

    @FXML
    private void createPackageAction(ActionEvent event) {    
        double d;
        ArrayList<Float> al = new ArrayList<>();
        //Parsing String to Float type for getting coordinates to Google Maps
        al.add(Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLatCoordinate())); //lat of start place
        al.add(Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLngCoordinate())); //lng of start place
        al.add(Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLatCoordinate())); //lat of destination place
        al.add(Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLngCoordinate())); //lng of destination place
        //Count distance for parcel
        d = (double) invisibleMap.getEngine().executeScript("document.createPath(" + al + ", 'red', 1)");
        //Create parcel depends on class choice    
        if(firstClassButton.isSelected()){
            FirstClassParcel fcp = new FirstClassParcel(nameField.getText(), (double)Double.parseDouble(lengthField.getText()),
                    +(double)Double.parseDouble(widthField.getText()),
                    +(double)Double.parseDouble(heightField.getText()),
                    +(double)Double.parseDouble(massField.getText()),d,
                    breakableCheckBox.isSelected(),
                    Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLatCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLngCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLatCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLngCoordinate()));

            if (checkParcel(fcp)) {
                Storage.getInstance().addParcel(fcp);
                // Window will be closed, if test is true
                closeWindow(createPackageButton);   
            }
        }
        
        if(secondClassButton.isSelected()){
            SecondClassParcel scp = new SecondClassParcel(nameField.getText(), (double)Double.parseDouble(lengthField.getText()),
                    +(double)Double.parseDouble(widthField.getText()),
                    +(double)Double.parseDouble(heightField.getText()),
                    +(double)Double.parseDouble(massField.getText()),d,
                    breakableCheckBox.isSelected(),
                    Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLatCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLngCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLatCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLngCoordinate()));
            
            if (checkParcel(scp)) {
                Storage.getInstance().addParcel(scp);
                // Window will be closed, if test is true
                closeWindow(createPackageButton);   
            }
        }
       
        if(thirdClassButton.isSelected()){
            ThirdClassParcel tcp = new ThirdClassParcel(nameField.getText(), (double)Double.parseDouble(lengthField.getText()),
                    +(double)Double.parseDouble(widthField.getText()),
                    +(double)Double.parseDouble(heightField.getText()),
                    +(double)Double.parseDouble(massField.getText()),d,
                    breakableCheckBox.isSelected(),
                    Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLatCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intFromButtonChoice).getLngCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLatCoordinate()),
                    Float.parseFloat(spm2.getSpms().get(intToButtonChoice).getLngCoordinate()));

            if (checkParcel(tcp)) {
                Storage.getInstance().addParcel(tcp);
                // Window will be closed, if test is true
                closeWindow(createPackageButton);    
            }
        }
        //Check if class is not created
        if (!firstClassButton.isSelected() && !secondClassButton.isSelected() 
                && !thirdClassButton.isSelected()) {
            Alert myalert3 = new Alert(AlertType.INFORMATION, "Please, choose class first");
                myalert3.showAndWait();
        }        
    } 
            //This method is testing size-, weight- and fragile limits
            public boolean checkParcel(Parcel cp) {
            // default retutn true
            boolean bReturn = true;

            // length
            Double d = new Double(lengthField.getText()); // length
            Double lenmax = cp.setLengthLimit(); // length max
            if (d > lenmax) {
                Alert myalert = new Alert(AlertType.INFORMATION, "Length is bigger than limit");
                myalert.showAndWait();

                bReturn = false;
            }
            // width
            Double w = new Double(widthField.getText()); 
            Double wmax = cp.setWidthLimit(); 
            if (w > wmax) {
                Alert myalert = new Alert(AlertType.INFORMATION, "Width bigger than limit");
                myalert.showAndWait();
                bReturn = false;
            }
            // height
            Double h = new Double(heightField.getText()); 
            Double hmax = cp.setHeightLimit(); 
            if (h > hmax) {
                Alert myalert = new Alert(AlertType.INFORMATION, "Height is bigger than limit");
                myalert.showAndWait();
                bReturn = false;
            }
            // weight
            Double we = new Double(massField.getText()); 
            Double wemax = cp.setWeightLimit(); 
            if (we > wemax) {
                Alert myalert = new Alert(AlertType.INFORMATION, "Weight is bigger than limit");
                bReturn = false;
            }
            // distance
            Double di = cp.distance; 
            Double dimax = cp.setDistanceLimit(); 
            if (di > dimax) {
                Alert myalert = new Alert(AlertType.INFORMATION, "Distance is longer than limit");
                myalert.showAndWait();
                bReturn = false;
            }
            if (breakableCheckBox.isSelected()) {
                if (cp.setMachineBreakOrNot()) {
                    Alert myalert = new Alert(AlertType.INFORMATION, "This class is not best choice for fragile object");
                    myalert.showAndWait();
                    bReturn = false;
                }
            }
        return bReturn;
        } 
}
