/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

import java.util.ArrayList;

/**
 *
 * @author Olga
 */
public class Storage {
    private static Storage instance;
    private ArrayList<Parcel> parcels; 
    private ArrayList<String> parcelsWithNames;
    
    private Storage(){
        parcels = new ArrayList();
    }
    
     public static Storage getInstance(){
        if (instance == null) {
            instance = new Storage();
        }       
        return instance;      
    }
     
     public void addParcel(Parcel p){
         parcels.add(p);
     }
   
    public Parcel getParcel(int i){       
        return parcels.get(i);
    }

    public ArrayList<Parcel> getParcels() {
        return parcels;
    }
    public ArrayList<String> getParcelsWithNames() {
        for (int j = 0; j < parcels.size(); j++) {
            parcelsWithNames.add(j, parcels.get(j).getName());
        }
        return parcelsWithNames;
    }

    public void setParcels(ArrayList<Parcel> parcels) {
        this.parcels = parcels;
    }    
}
